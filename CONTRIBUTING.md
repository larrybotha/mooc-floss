# How to contribute?

We strive to welcome all contributors! 

We want to be a project where contributing is both simple and inclusive, so please read the [Code of Conduct](CODE_OF_CONDUCT.md) (tldr: "We pledge to act and interact in ways that contribute to an open, welcoming, diverse, inclusive, and healthy community.") first. You may also want to read the [license](LICENSE.md) (tldr: CC-BY-SA 4.0). 

Then, the preferred way to contribute is to submit a Merge Request on Gitlab. You can fork the project, edit in a branch of your fork, and present and submit your modifications.


# So, what's the plan?

We tried to create this course as openly as possible, so that even after the start of the project, it's still possible to understand the plan and the reasoning behind decisions taken. In that spirit, all our meetings are recorded and transcribed. In particular, we had two preparatory sets of live meetings about this course:

 - From February 2021 to March 2021, we brainstormed the contents of the course, aka "What are we going to talk about? How to approach it?"
 - In April and May 2021, we tried to put these thoughts into a *structure* for the course: What steps will the students have to take, what sort of activities will they do, and what we will expect them to learn.
 - From there, we are now (May 2021) focused on creating a first draft of the content, for all modules, and all module chapters. The first draft will be created in a `${week}/brainstorm.md` file, then split into the actual course structure within `course/html/` folder.

# That's great! Can I help?

Yes! In each module, each chapter has someone assigned to it. If you're willing to help create material for a course section, there is usually an issue about it. Just post to this issue, and get in touch with the coordinator of that chapter, and they'll tell you what they have already done and what you'll be able to help with!


# Communication

We communicate primarily on three platforms:
 - Gitlab: All long-running discussions happen here on gitlab, whether it's for discussing a problem or a planned change (in an issue) or discussing a merge request
 - Video chats: Most of the brainstorms and plans we've made were organized by videoconference, which is usually faster to communicate and exchange ideas than text. Since it's harder to backlog, they are all transcribed to be browsable and searchable. The times and links of those meetings are usually announced as an issue on Gitlab with a deadline
 - Matrix: We have a public chat room for more informal discussions or to exchange links, but it's less used than the other solutions. The room id is #mooc-floss:matrix.r2.enst.fr

Communication primarily happens in English (as the course will be in English). Some French may slip in as many course creators are French.



## Decision process

Ideally, the merging decisions will be taken based on [Consensus](https://en.wikipedia.org/wiki/Wikipedia:Consensus). The main difference with Wikipedia is that the modifications are approved before they are merged, while wiki editing is (mostly) post-publication consensus building, with the MR page as discussion space. 

In the case of major disagreements, ultimately, **for now**, the decision process ends with the <abbr title="Benevolent Dictator For Life">BDFL</abbr> process from the initiators of the project (i.e. Marc Jeanmougin), but once/if the community grows, more democratic processes could be discussed and adopted, if deemed beneficial for the project's health.




