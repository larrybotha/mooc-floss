# Brainstorm - Module 4 - Meeting 2 Transcript

Video: https://digimedia1.r2.enst.fr/playback/presentation/2.0/playback.html?meetingId=73d3cd53ec8ede3f636b9a782ecc9f6375cf2271-1620060633327

Marc: 00:00:02.696 Okay. Welcome everyone to this fourth session of transferring the brainstorm results into activities and lists of items for the MOOC. So today, we are on week four, so I've put the link in the chat of the calc where we put the items. I will also put the link of Xavier's compilation from transcripts. So the main idea for this week is to talk about the social aspects of contributions. So we can talk about things like-- well, if we see what Xavier compiled, we talked a lot about code of conduct, about burnout, and the problem of burnout in FLOSS communities, retention in project, people who stay or drive-by contributors, and so on. I will see if I've--

Xavier: 00:01:12.734 By the way, I don't think that's a full compilation of the brainstorm. That was just a few points that were, I think, maybe in the first part of the meeting or something like that. I don't think I've completely finished a job on that one.

Marc: 00:01:27.120 Yeah. If I look at what we have also discussed, it was also about community management in general, about how to contact, how people discuss in projects, which means talking about IRC, mailing lists, and things like Zulip/Rocket.Chat/Mattermost/whatever each project uses, and also talking about the diversity of contributor types which means just not coders, but also other people that interact with coders like designers, like bug reporters, etc., and how to interact with other types of contributors. Not everyone does the same thing in projects. And that sort of discussion points.

Marc: 00:02:32.249 So we finished the module before with asking people to contribute, in particular asking people to contribute to Wikidata or Wikipedia, but more likely Wikidata. And we have talked a lot in the previous week about the organizations, the types of organizations, and how they organize around-- we talked about licenses. We talked about economic models and actors. And so now we come and we want to talk to students about how people interact within projects basically, or maybe across projects. So what would you think would be a good introduction to this matter? Ideally an activity but we can change. It's more engaging to have activities at the beginning of a module.

Xavier: 00:03:49.836 Maybe I will have a question on that maybe for Kendall or maybe [inaudible] because you weren't in the first brainstorms, so maybe you have some ideas of things that you do that we are not aware of right now. So in terms of activities for contribution, besides the one that we've talked in previous weeks, are there things that you found useful to get people to get into the flow?

Kendall: 00:04:18.370 So since we have that sandbox repository for messing around and making sure that all of your git config is set up correctly, we usually use that instead of going outside of OpenStack. But I think Wikipedia is a really easy way to start because that is, I think, actually my first contribution to any open-source thing. It was a part of a course I had at the University of Minnesota. And so setting up an account for that, you just need one account, and you go and make your change, and it needs to be reviewed by other people. So I think that that simulates the whole experience pretty well. And it is nice because there's a huge breadth of things that people can contribute to in terms of, "Oh, I know a lot about frogs," I don't know. Some random thing. It doesn't have to be about something related to open source obviously. It could be any topic that people have extra knowledge or whatever, or maybe they just found a typo in a Wikipedia article and want to fix that. So I think that that's a good example, but we have talked about having a sandbox equivalent in GitLab setup, so. Yeah. I don't know.

Olivier: 00:05:52.537 May I?

Kendall: 00:05:53.987 Yeah.

Olivier: 00:05:55.659 I think that sometimes-- actually, you mentioned sandboxes. Sometimes projects do provide sandboxes for people to feel comfortable without breaking anything and learning the tools and so on, but it is probably different what you find in Wikipedia and what you can find in most open-source projects, where I'm not sure every project has a sandbox, and most of the times, you need some kind of permission to be able to use the tools or to share something and so on. Maybe discussing is a bit different because most of the times, you can connect as a user and discuss freely without any permission and so on. But contributing is often behind barriers, and you have to perform some steps to overcome those barriers. Whereas in Wikipedia, the barriers are just technical, but, as you mentioned, you just create an account, and then you can do whatever destruction you want, because it's not a problem. It's meant to be that way. So the difference is probably important, which justifies that you must discuss before getting granted some permission. So which motivates the need for discussion venues for getting yourself known for asking for permission or privileges, stuff like that.

Xavier: 00:07:33.480 Actually, yeah. The point of permissions or not permission can be a good one, and maybe we could be looking for other types of contributions that don't require a permission, that might be also something to develop a little bit. We've got them to post on the forums, but actually contributing on the forum is one of the easiest form of contributions, right? Looking at threads which are missing an answer and etc. And usually that's something that people who manage the project are really grateful for because there are always way too many questions to answers. So especially if they pick a project that they are familiar with, there might be some question that they can answer. And if not, digging into some questions might be a first step into going into the code, installing things, and etc. Maybe something like that.

Olivier: 00:08:39.212 Yeah, some descriptions of the different levels of permissions and the different level of contributions, and probably all projects are not the same. Maybe some have a forum that requires you a few steps before you're able to post a URL and stuff like that, which is both technical and probably both trying to convince you to learn before messing around.

Xavier: 00:09:08.988 Another intermediary form of contribution can be around the documentation because that tends to be also something that has less people on it. The maintainers are often less time or even interest for it. It's already closer to the code because it's often in git or something like that. There is less of strict constraints about code formatting or rules of the project and etc. And yeah. So I've seen that working quite well, not only just as a first contribution, like fixing a typo and etc., but picking some documentation tasks actually or asking what needs to be documented and starting to work on that. Because then you get all that social aspect of going through pull requests and etc. You might even also get further into the code because if you want to document things well, you need to actually know what's happening. So that might also be another activity that we could give them to go further down the rabbit hole. You don't like that, Olivier?

Olivier: 00:10:29.471 I think it is quite diverse between projects. Some projects do use wikis or tools that allow easily to fix a typo, add an image, contribute to the documentation. But others, the documentation is sometimes not managed like that in the open. It is fabricated. It is compiled. So it becomes something that is closer to code that is hard for a beginner to install all the tools. Like if you think about Sphinx with Python, and sometimes translating is hard also because there is no translation infrastructure in place or you have to monitor different tool suites. Sometimes there are good contribution guidelines and so on, but sometimes it's harder. And also contributing to documentation sometimes is quite sensitive because the way to explain how something works and the steps and so on, you need to be an expert to be able to explain. So sometimes for newcomers, it is a bit hard to say, "Oh. Your documentation is crap and I will fix it."

Xavier: 00:11:38.917 But that was part of what I was suggesting here. It's not what I would suggest necessarily at the very, very first thing, like the very first thing where we had some of those types like posting in the forum, a typo. These kind of things or even like--

S?: 00:12:01.373 [foreign].

Xavier: 00:12:06.409 Do you hear me again?

Remi: 00:12:07.870 Yes.

Xavier: 00:12:08.069 Okay. Sorry. That's the connector which is-- so yeah, what you mention is part of what I like about this, is that it's intermediary. In a lot of projects, it gets closer to code. So it helps with the step of going from something super easy. Posting on the forum is super easy, no actually. But at least from the standpoint of what we are talking about here it's like that. There is no barrier for that. To something that is heavily gated which is like a code contribution. And the documentation tends to be somewhere in the middle, where you have to set up some kind of developmental environment. You have to adjust to some rules and etc. But on the other side they tend to be a bit lighter often. I mean, of course, it depends on the project. And also like you said, you have to take more of an expert perspective. But the beginner point of view is also useful because someone who reads the documentation for the first time, who doesn't know anything about the project, will also have a good eye to explain that to someone who is not one of the core developers. So I do agree that there are difficulties, but what I like is that, yeah, it helps getting on that step to get to the code contribution.

Marc: 00:13:32.027 I also think that code contributions can be simpler than documentation contributions.

Xavier: 00:13:38.316 Yeah. It depends I guess on the--

Marc: 00:13:40.070 It's not the same [crosstalk].

Xavier: 00:13:41.416 --exact code in the project. Yeah. And it depends on the person also I guess because some people will be better at writing text. Some people would express themselves more with code. It's [tricky?].

Olivier: 00:13:53.173 One of the difficulties is if documentation is managed like source code. You can fork documentation. You can make a pull request. It can be accepted. And so on. So you can try experiment, and eventually, contribute something that is accepted. But sometimes documentation is in a wiki for instance. So in a wiki, it's easier maybe than to code, but then there is one single page in most of the wikis. You you cannot fork and be approved in a wiki. It's all or nothing, more or less. Of course, there are history in wikis and so on. So sometimes it's harder. Sometimes it's easier. Because it's easy to break documentation if you edit the single instance of documentation. But okay, let's not dive into the details. Probably that's not relevant for today's meeting. But that's why sometimes it's [easier?]. Sometimes it's easier but risky, so.

Olivier: 00:14:55.443 Something completely different maybe, about retention in projects. I don't know if it is really something we could discuss. I haven't had a look at the paper by Fitzgerald, [inaudible] and so on that is mentioned in the transcript. But I remember nice-looking dashboards where you can see generations of developers, generations of contributors, and stuff like that. Does it look like something familiar to you?

Marc: 00:15:38.473 No.

Olivier: 00:15:39.836 So I would have to research that a bit. But maybe you can find that with a simple search. So it's an image where you can see people's activity like a histogram. And you can see the whole duration of the project with people that come and go. You can see they are pretty much active and then they-- I should do it the reverse way, from right to left, left to right. And then you can see the first generation of contributors, the next generation, and so on. And so you can see several phases of the project and people that come and go and so on. So it could be interesting to use that as an illustration or as maybe an activity, try to plot such a diagram, something like that, that could give a figure-- that could give a hint on the fact that the project is a living community and that every project is different. Maybe there are project where people, they contribute for years or projects where people come and go and stuff like that. And maybe there is a typology or social sciences can categorize the projects and so on. So maybe that's kind of visual illustration that could give something interesting to support activity in this week. I don't know if it is particularly useful in terms of when I'm new to the project I don't care. Maybe I'm just scratching an inch and that's all. But when you're a newcomer and you look at a project, you probably wonder if it's a welcoming community and so on.

Xavier: 00:17:38.925 Could be worth maybe at least mentioning it that there can be different types of projects for that. In terms of activity, another thing that I thought of in another type of intermediary contribution is reproducing bugs, and maybe the step after that which would be maybe writing a test that reproduced the bugs. And then if someone managed to do those two steps, the third step which would be to fix the bug is very facilitated because it's the [part?] bug report on an issue that has been verified and for which there is a test. So yeah, again it might depend on the project, the status of the test, the bug report in question, and etc. But could also be steps toward a contribution.
[silence]

Olivier: 00:19:06.609 I think you mentioned a valid point, Xavier. I was just looking for some illustration of my previous point, but I cannot find it.
[silence]

Marc: 00:19:31.487 Yeah, I think Polyedre is right. We should try to find like three or four chapters for this week that are the main points we will talk about and try to go in-depth for each of them. And first think about the big picture, which is what do we want the students to learn in this module, and what do we want them to do, and what we want them to take home, basically. And what we want on them to do for the project, which is in this case, the project phase at this point is, make sure for the project you chose to get in touch with the project, to know how it stores bugs, how it stores discussions, where it discusses, and to engage conversation with the project. That's basically what the students should be at at the end of this module for their main project.

Remi: 00:20:31.773 And they have to update their [foreign]?

Marc: 00:20:41.593 Their main project.

Xavier: 00:20:43.984 The journal, yeah.

Remi: 00:20:45.011 The journal, yes. So to help them, maybe we can have a list of questions they can ask themselves like, I don't know, "Is there an IRC channel or another chat or a forum discussion?" All of the questions that we have in mind, we should put them in a document to help them finding how to contact the team.

Xavier: 00:21:19.131 Yeah. Actually, a lot of those things will be steps that have been discussed or maybe even done in previous weeks. It depends if they directly pick the right project or not. So maybe we'll need to handle that, both different cases where maybe some people will pick immediately the right project. They will already be in a good place everywhere, maybe not. And then in which case they need to pick another project and redo that assessment for that. And maybe we need to provide ways to help them to make that assessment, okay? The project that you talk to in the previous weeks, did that go well? Did you get answers? Did you get an idea or not? Maybe like some kind of rubric to grade the project. I don't know. That could be a good way. Like on those elements of checklist that mentioned, Remi. Maybe giving a score every time, and then if it's very low score then maybe they need to pick a new one. If it's good score they can keep it if they want to. And if needed the journal might be useful for that. I don't know who would handle the journal if they switch. Maybe they could have like different project listed in there and then pick the one that work the best, something like that. And yeah, this week could be about revisiting all of those things that have been done little by little and maybe redoing it with their final project if needed.

Remi: 00:22:55.760 That's very interesting. So I've put in list-- I mean in the table something like grading the project, go or no go, and maybe week four-- I mean, this module will be the last chance to choose another project.

Xavier: 00:23:19.497 Yep. And maybe one thing also is that we'll have partner projects, right? So there could be like a backup if really they only went to that projects or whatever. Maybe that's also a good place to say, "By the way, if you're really lost, so there are those projects where you might find more help," etc. But yeah, I like the last chance like the deadline bit on picking a project.
[silence]

Marc: 00:24:00.634 So what do you think we should start with? We can start with the project if we want, but it's probably better to talk about the communication channels, like the classic communication channels that exist, before asking people to join those channels.

Xavier: 00:24:20.483 Well actually, I'll ask them to join some of those channels even on week one, right? So maybe we can recap and develop that. Because it's true that on previous week there wouldn't have been a lot of space to it. But that will be true of most of the things I think we'll be asking them to do this week. They will have done a little but of something, but maybe that can be the place where we go more in depth and recap like the different types and detail. And maybe in each of those sections when we develop like communication channels. I don't know what else we have to recap actually in this week. But every time detail that remind them that they are to assess their current project and maybe provide also that rubric for grading that. That might be a moment too where it's not just a quick explanation in passing that people should be responsive. We put real criteria like how long exactly they took to respond. Do they have recent merge requests and etc? And maybe detail that there. So maybe this week could be a week where the kind of what we have in project tips, expands for the [how?] module, maybe? I don't know if that's a good idea, but that could be an idea where we kind of review everything that they have gathered through the previous weeks and go a bit more in depth and get them to assess it. And we do one section for each of the criterias for evaluating the project, maybe? I don't know.

Olivier: 00:26:20.241 Okay. So that's mainly for people that are hooked and that are really in need of performing because they feel confident that they-- or they have the need to contribute to projects and so on. And maybe for people with less motivation, there is still some need for novelties. Not just in-depth, more in-breadth. I don't know.

Remi: 00:26:52.300 What do you mean? Because for week two it was optional for example. For experts that already know git, they could go just-- if they pass all the content then they go directly to the quizzes, and they go fast to next module. Is it the same thing you are saying here?

Olivier: 00:27:12.889 No. I'm thinking about people that are casually interested. And maybe, okay they found a project. They have a rather good interest, but okay, maybe it's sufficient for them. They would want to learn stuff instead of practicing. I don't know. Learn different stuff. But maybe that's the whole point of the week to go in-depth. I don't know.

Remi: 00:27:39.408 So the goal of the MOOC is to contribute within a code repository or a code change somewhere. So we have to be focused on how to contribute to a code line or somewhere to change the code. It's not only learning about the project. I don't know.

Xavier: 00:28:10.443 It's true that if we give the impression that the contribution part is an optional part we might end up with a bunch of people who went through the whole course and actually still don't know how to contribute. So I mean, of course, people do what they want, if they want that.

Olivier: 00:28:24.934 They know, but they may not want to practice now. You know what I mean? Or they find a project, but that's not the big project they want to-- it's like, "Okay, I want to know more. Okay. I've understood, but it's not the proper moment to practice," and so forth. Okay. Forget about it.

Remi: 00:28:44.730 So you mean we could have dropouts in the middle of the MOOC for this kind of person?

Xavier: 00:28:51.986 No. There are never any dropouts in MOOCs. I don't know what you're talking about.

Olivier: 00:28:57.159 No, but in every MOOC there are just a few people that complete the whole course, but.

Xavier: 00:29:06.867 No. It's true that at the end people will do what they want. So being accommodating, that's fine. For example, just letting them skip if they want. But I think we can still have kind of a recommended approach. And that's where I follow what you say, Remi, is that the more we encourage people to actually do, maybe the more people will actually do it and probably the more people will actually be learning something at the end. Because I think really contribution is something you can learn it in theory. But until you've actually been in front and done a contribution and felt like the sweat of pressing the submit button, I think you don't really know what it is actually.
[silence]

Xavier: 00:30:07.243 That said, it's maybe not completely exclusive what you were saying, Olivier. Are you thinking of some specific points that would fit within the current week that would be good to add because that's not a bad thing to add, the additional knowledge?

Olivier: 00:30:24.079 Well, it very much depends on the projects and how they're picked and how responsive they are and so on. So there is quite a lot of uncertainty on the response people will have and the quality of the experience, but I agree, it is central point. So probably good as it is. Keep on.

Xavier: 00:30:49.814 Okay. So do we go for that structure then for this module of that kind of they're assessing the project and taking the decision as the main activity for which we dedicate several sections? Do you see another way to approach that?
[silence]

Xavier: 00:31:29.993 The silence may mean yes or boredom, depending. Polyedre, what do you think about what you've heard so far? What would you like to have around the time where you pick your first contribution in a project that kind of help, advice?

S6: 00:32:02.619 Okay. Maybe a list of tips. So question I can answer to know if the project is easy for first contribution or to find what to do. So for instance in GitHub we have [inaudible] in GitLab. I don't know about the learning curve. Maybe you can expand on that, Remi.

Xavier: 00:32:52.322 For the learning curve you mean how difficult it will be to get to the point where you can contribute. Is that what you mean, Polyedre?

S6: 00:33:01.493 No, it [inaudible].

Xavier: 00:33:08.293 Polyedre, you might have something on your computer that takes a lot of CPU because you were really breaking off.

Marc: 00:33:15.900 Choppy.

Xavier: 00:33:16.667 Yeah, choppy.

S6: 00:33:18.485 Oh, okay. No, so I just read what Remi wrote on the chat, and I don't know if contributing to the Linux kernel is out because I don't know the process, and [inaudible] sounds terrifying for me. But okay.

Olivier: 00:33:50.084 I'm not sure I got Remi's suggestion about the learning curve. But I'm not sure you're familiar, Polyedre with what the learning curve is.

S6: 00:34:02.438 I know what the learning curve is, yeah.

Olivier: 00:34:05.817 Okay. I was thinking about the learning curve and the fact that we have one week to do stuff and get responses and get some kind of feedback or approval of what we do and stuff like that. Actually, the learning curve depends probably of the reactivity of the community of the project. In particular for asynchronous activities. So we can have a problem if people are just in vacation or too busy or stuff, and people just don't have a response in time. So in activities, it is probably important to distinguish what could be done synchronously and asynchronously to make sure that there is no frustration if people don't respond. Something like that. Of course, the way that you ask questions, the way that you present your contribution, is a way to get better feedback. But still the timing issue is probably critical here. I don't know if you've talked about it already in previous weeks.

Xavier: 00:35:28.483 Yes, that's actually the very reason why we start very early with this because we know that it takes a lot of time. So that's the whole reasoning behind, yeah, on the first week starts on posting in the forum and etc. But because we want to give an opportunity for people to change their mind or make a wrong choice that's why we have this week. But it's kind of like people make their first post in their project of choice this week. To me, they are a bit late already because there is already halfway through the course, and they have missed big opportunities to get themself known in the project and etc. So I think the more we can emphasize what you just said early in the course, the better. But obviously, we have to--

Olivier: 00:36:11.782 And then is it feasible to, in the journal that was mentioned, create a curve like temperature curve when you're in a hospital? But just to avoid the frustration related to the time spent. The steps on the x-axis, on the time axis, wouldn't be the days or the hours. It would be the number of messages on the list or the number of messages on the forum or something like that. Which is not the time spent, the time you will expect to see something happening, but more the ticks in the project's life. So, oh, I noticed 10 messages on the mailing list, and no one answered. The project is alive, but I got no answer. So that's frustrating. Whereas nobody says anything, nobody answers either. So I just have to wait. So if we speak about-- so that's the learning curve. But it's still the curve of activity of the project and the delay that you are expecting to get your contribution noticed. Something like that.

Xavier: 00:37:34.433 It's true. Using several indicators like the frequency of replies and etc. to determine an ETA for getting to the contribution or something like that.

Olivier: 00:37:44.041 Yes. And should I worry, or am I just to-- I'm not in the right pace, and that's normal. So trying to assist that somehow, maybe it's hard work. I don't know. But in addition to the journal where people log and so on, maybe you can try and have a watch of the activity of the project in the venue where you are expecting some feedback. And so that may help understanding things.

Xavier: 00:38:22.606 Yeah. That's true. It's true that there are different aspects to consider, length of timing the activity being one. Think behind learning curve, there are different things also. There is the technical learning curve. If I've just started learning how to code and I try to contribute to the kernel, that might be tricky for technical reasons. But it might be - I don't know; I've never made a contribution there - that they are actually really good at managing contribution and accepting them, which doesn't seem absurd given the number of contribution there is in the kernel. So maybe the technical learning curve is pretty steep, but actually the contribution one might be a fairly smooth one. And maybe in another project, that would be a different thing. So maybe there are different things like that that we could evaluate and also maybe match to the profile of the person trying to contribute. Because a beginner in code will not necessarily be choosing the same project or the same contribution as someone who is really experienced. And same thing for - I don't know; we were talking about documentation earlier - people who like to write, who are good at writing documentation and taking that perspective, might be better at that, while maybe someone who doesn't like documentation, doesn't like to talk to other people might choose a different type of contribution also.

Remi: 00:39:52.872 Also, I was thinking about mentoring. Maybe it's also the last chance to choose for mentoring for the rest of the course. And maybe we could have some kind of system to invite open-source projects to contribute to the course with a mentoring system. Like Tinder, yes. [laughter]

Xavier: 00:40:29.588 That's a really good point. We haven't talked enough about mentoring from the beginning. And that's actually a pretty critical part.

Remi: 00:40:38.213 Or if any open-source project wants to be involved in helping the students we have in this MOOC contributing to their software, maybe that's the module where it has to happen.

Olivier: 00:40:56.728 And bids, auctions.

Xavier: 00:41:01.057 Auctions, bid for the mentor. I mean, I think same thing. It's the last limit for mentorship to happen. Ideally, that happens at the very beginning I think, a bit like in Upstream University. So yes, I think it would have to be something to include here. But ideally, we would also have something a little bit earlier. I mean, the fact that we have lots of different possibilities of projects of type of students or whether like even potentially have a paid version of the course with a dedicated mentor versus volunteers for the free version, that creates lots of different scenarios and possibilities. So probably we'll need to have a breakdown of all of that. But yes, I mean, whether a project has mentors or not might be also an important criteria here to evaluate and to research from the beginning. Because I mean, some projects already naturally have them even outside of what we do. Maybe some of the project, maybe especially the partner project, might have mentors because they actually want to participate and encourage that. That said, there might not be enough mentors for everyone, so I guess that's Olivier's idea of bidding for the mentors. But yeah, that part might be tricky. And we might also want to look at mentorship from the course itself because maybe especially when there are several runs of the course maybe some more experienced people will be able to guide the newcomers. So maybe we--

Remi: 00:42:52.666 Maybe we can ask the students to find a specific person to help him/her if no mentorship was found or I don't know.

Olivier: 00:43:08.116 I do agree that in several MOOCs, we have seen previous year students helping new ones and so on and some teaching assistants promotion for the best contributors in the previous editions. Okay. But we have to bootstrap first without them somehow.

Xavier: 00:43:32.659 Yeah. I think what seems clear is that no matter how good we are and even with the later version of the course, there probably will be more students than mentors. I mean, it might be that we manage to get enough mentors for everyone, but that would be an ideal scenario. So it might be good to have a backup solution for that. And I don't know if that's what you meant here, but maybe pairing the students together so that they get to approach a project together and review a bit each other. Especially maybe with the journal, we have talked about peer review. Maybe that can be a more continuous way of doing that, that gets closer to mentorship. Or maybe just finding a friend, someone they know that might be able to help them with that, someone who they know who has a few contributions. It's difficult because the role of mentor is not just-- you can't just be a normal contributor. You have to know how to guide it too. But yeah, that might be something that we could do around that is, yeah, to try to have the students helping each other.

Xavier: 00:44:50.782 Or I think it's something we had mentioned also the previous weeks is to maybe group the students who want to contribute to the same project, and maybe whether it's on the forums on a dedicated thread or like a workgroup or something, having them like helping each other, reviewing each other, like figuring out information in common. Maybe also, that can be something that links into the Wikidata contribution because the information they gather will be stored there. So maybe things around like what we were saying before like the rating of the ease of contribution, or the frequency even of replies that you were mentioning, Olivier, maybe that's something they work on together. So yeah, there might be a few things like that. It's never going to be as good as having a good mentor that follows and is able to correct the direction and give advice, but that might be a good backup.
[silence]

Remi: 00:46:07.928 All right. What else do we need for this module? I think we already have a lot.

Xavier: 00:46:22.188 There is a lot, but maybe it would be good to actually try to look at precisely what would be the outline. So, Marc, I don't know if you already put that in place somewhere. But yeah, to see how we structure it.

Marc: 00:46:39.767 I've put items in the Framacalc. But it's not ordered and it's not detailed. And also most of the things that are listed are teaching items and not activities or other equivalent stuff. So we might need to think about, can we do an activity about when we talking about IRC and mailing lists, is there an action item we want to ask? And is it an action item about a project they want to contribute to, or is it something unrelated?

Remi: 00:47:21.777 Maybe, I know it's difficult to have, but can we have study cases? I don't know how we say that in English. [crosstalk].

Marc: 00:47:29.569 Case studies.

Remi: 00:47:31.118 Case studies. For example, for the code of conduct, maybe we can find some very common case study, and we ask them to read a little text or something that describes the case. And they have to answer a few questions, what happened in that case. I don't know.

Marc: 00:47:53.130 So basically reading about code of conduct somewhere and then answering an MCQ about it?

Remi: 00:48:01.048 Not only reading a code of conduct, but also looking at a real situation that happened, that was out of the code of conduct.

Olivier: 00:48:11.117 You mean when Linus Torvalds says profanity in the Debian conference in an American state where people are famous for their bigotry?

Remi: 00:48:21.097 Yes, for example, yes.

Marc: 00:48:22.756 If it was about [inaudible] you're allowed because--

Remi: 00:48:27.891 But I'm not sure if it is easy to find real situations that occurred, maybe.

Olivier: 00:48:36.230 There are plenty of publicity about - how would I say? - nasty situations, but then the day-to-day behavior on be nice to people and not making mistakes and so on isn't widely reported by the press, of course. So of course, my example was irrelevant.

Marc: 00:49:03.995 I also think that it might be problematic to point at a specific case and say, "Well, can you research the case where unknown contributor 700 of this project did something bad," and point potentially 100 people at this case and asking them to research and ask questions. And yeah, that was five years ago, so yep. I don't think [crosstalk] a case study [crosstalk]--

Olivier: 00:49:33.650 Maybe analyzing differences between different code of conducts could be something that-- why is that that they have several provisions and so on, but it's probably--

Marc: 00:49:50.925 It's usually a matter of personal preferences between those who want every possible case listed and those who want general guidelines.

Olivier: 00:49:56.248 Yes, or particular history and trying to address some particular event that was related to a particular person and particular situations, so on, so on.

Xavier: 00:50:08.232 For specific cases, you could always consider anonymizing the case, not getting them out and going to ask the whole planet about that because that doesn't seem fair. People need to be able to be forgotten when they make mistakes too. But still having concrete scenarios can be useful, I think. There is also the fact with code of conduct and etiquette in general, it's not just about not doing profanity and etc., but it's how to approach a collaboration and discussion relationship. So it's things like making sure you read the mailing list a little bit before posting the first time that you don't have a big blob of quoted text everywhere in your forum posts. That there are lots of things that are not necessarily about bad behavior but just bad form and not thinking about how other people are going to receive the message. Then I think on that there is a lot that will be related to the likeliness of getting their contribution accepted because if they are rude or make like racist comments on the forum they are of course going to be kicked out, but I don't think that's going to be the majority of people that's going to be [crosstalk]. On the other side--

Olivier: 00:51:37.568 It is--

Xavier: 00:51:40.455 Just to finish with that. On the other side, people who arrive and don't pay enough attention to kind of how to interact with other people, how to observe the unwritten rules also, they are much more likely to just even be ignored and ghosted in a way, so.

Olivier: 00:52:01.156 And I think the question of the code of conduct is of course it's legal or paralegal commonly agreed thing that can help resolve nasty situations. But in general I would tend to think that it is very much applied on newcomers and not always applies to olders. And as in every community there is tension sometimes and there are fights and there is a life and not everything is happy and pink. And so sometimes you have to be passive-aggressive. Sometimes you have to confront others. And sometimes they are cliques and disputes and so on. So the code of conduct in principle applies to obvious situations. But there are non-obvious situations where of course you have to have all the background and understand who speaks about what and all the unwritten elements. So depending on the projects and the venues, analyzing what happens on the Debian mailing list or on the Linux kernel mailing list and stuff like that is something that you cannot refer to a code of conduct and say, "Oh, this post was against the code of conduct," because it's more than that. It's more complex. You need the history. So I wouldn't insist too much on code of conduct because it's a tricky part in real life in most of the projects I know.

Xavier: 00:53:59.103 Yeah. Written versus unwritten rules. I agree. At least mentioning that--

Olivier: 00:54:03.731 But asking questions the smart way or stuff like that, which somehow relates to how to behave when you're a newcomer. That's probably much more practical.

Xavier: 00:54:21.442 Yeah. And I think that that might be old school, but maybe either as a replacement or in addition to maybe a case study on the code of conduct there could be one on netiquette. We could take a reference text from the '80s or the '90s on how to behave on a news group or whatever, or even something more current. I don't know. But I think those kind of rules of how to behave in a community, I think might be worth taking a reference and getting them through that. This one for example.

Olivier: 00:55:07.274 So of course, it's Eric Raymond, which opens a new can of worms, and I'm not sure it applies to synchronous communication like Slack or Matrix and stuff. Was probably more about forum and mailing lists, given the--

Marc: 00:55:29.212 Good guidelines. Also, the form of writing is questionable.

Xavier: 00:55:40.598 Yeah, there might be some more up to date ones, but something in that. Okay, Ildiko. No worries. She has left already. But yeah, there might be something like that, that we would be able to use.

Marc: 00:55:57.747 Yeah. Basically, when we mention communication within [inaudible] communication channels, we want to tell people what are the implicit rules about communication channels like when you come into, for instance Slack like thing, you don't at all people just to ask a question or to say hi or to-- things that may be obvious, but it's better to state it like, don't do that because it will look bad for people who are not used to some specific method of communication. For IRC, for instance, don't ask a question then leave because you won't be able to see the answer. Thinking about, maybe telling about things that we think are obvious. Like mailing lists, don't write too long emails except if you have to. Don't send an email that will bother lots of people if you don't have anything to say in it, etc. What do you mean, Olivier?

Olivier: 00:57:12.121 I was meaning, like in very old role playing game named Advanced Dungeons and Dragons, there were alignments of the characters. So we could play a role, and people have to understand, are we just a company trying to get free support? Are we a new coming student asking for someone to do his homework? Are we trolls trying to harass women? I don't know. So we post messages and people have to figure out.

Marc: 00:57:57.946 You went to simulate discussions that can happen and when people [crosstalk]--

Olivier: 00:58:00.780 Yes. Of course, we are not going to pollute any real project venue with such messages which would be bad behavior.

Remi: 00:58:08.169 We could do that on our GitLab instance.

Marc: 00:58:17.016 Or we could just stage it in the course with, "This is a discussion that can happen, and can you explain what happens in these discussions? What's the goal of such newcomer in the discussion, etc?"

Olivier: 00:58:32.996 I don't know if we mentioned that, but we could use some sort of Among Us or another game where you had to find who's in the good guys and who's in the bad guys. And so we have two teams competing to each other. We distribute roles. And so they're the ones that are very cautious about the code of conduct and guidelines. And those who are just throwing nasty [inaudible] contributions. And you have to sort out who's who without knowing-- yeah, you know who you are.

Xavier: 00:59:14.117 Yeah. We also have to be careful about the black and white approach also because often in real situation, it's a lot less clear-cut. But I think being able to recognize those things. Maybe another approach for that activity would be to have kind of like choose your own adventure type where you have a few posts and you can choose your answer to that. And depending on how you react, you will get, I don't know, the maintainer banning you. In another case you'll get your contribution accepted or something like that that gives you an idea of the consequences of the attitudes, I don't know.

Olivier: 01:00:03.259 Probably hard to devise, but interesting.

Marc: 01:00:05.833 A visual novel basically.

Remi: 01:00:07.862 I have a very simple way of doing that. Let me put the link, ink, inker. It's a language to write this kind of--

Xavier: 01:00:32.059 The interactive fiction. Yeah. That could be really nice actually to do something like that. Like you said, it's a lot of work, but that would be cool.

Remi: 01:00:43.789 If we do a very small use case, it could be interesting.

Xavier: 01:00:50.088 Yep. I'd love that.

Olivier: 01:00:52.757 It could even be an activity for students to devise the next steps. We provide the first two chapters, and then we let them invent the third one given the orientation of the project they want to have.

Xavier: 01:01:12.060 Nice idea.

Olivier: 01:01:13.519 Maybe that's probably some kind of psychology, sociology MOOC instead of FLOSS contribution MOOC, but.

Marc: 01:01:22.484 But yeah. A visual novel would also be able to introduce the concept of diversity of contributions since you can have discussions within a project with people who are not code contributors and can take part in the discussions.

Xavier: 01:01:42.315 Kendall, you were trying to say something. I saw you on mute a few times.

Kendall: 01:01:47.352 I was just agreeing that if we kept them really small, one interchange or two interchanges, it would be more feasible than an entire story and conversation. And I do like the idea of providing one or two and then having them come up with a third one. Especially if it's, I don't know, focused around the code of conduct for the community that they're interested in just to kind of apply their knowledge or what have you. Yeah.

Xavier: 01:02:22.734 Yeah, it's true.

Olivier: 01:02:23.734 It's probably safer to do such an activity where it's constrained than what I was suggesting like trolling in forums where the experience for the students could be really bad if people tend to be a bit too trollish.

Xavier: 01:02:41.446 Yeah. And of course, everyone loves the bad roles because that's what's fun to do, so we don't be teaching the right things I think.

Olivier: 01:02:53.211 In the middle of the conversation, I posted in the discussion a link to Karl Fogel's Producing Open Source Software book which is probably more recent than Eric Raymond's, and it seems to be addressing some parts of the discussion because it mentions code of conduct and stuff like that. But it's more in the perspective of the project manager than in the perspective of the contributor of course.

Xavier: 01:03:24.003 Yeah, but in general I think we could use that book a lot more actually a bit for the reason we mention in a previous the week is that between the role of the contributor and the project manager is kind of a mirror thing where the good practice as a contributor also tells a lot about how to select the project and how to behave as a maintainer. And how to behave as a maintainer is also how to recognize the good contributor. So there is a lot of things that we can probably take from there. And, yeah, I agree that that section is probably a good one.

Olivier: 01:04:05.833 And there's also a chapter on communication that probably addresses a lot of those elements too like recognize rudeness, tone content stuff like that, avoiding common pitfalls like the smaller the topic, the longer the debate and so on.

Xavier: 01:04:38.512 Yep. And actually, that might be something to remember that Karl Fogel might be a good one to interview too. [I know?] I probably want to add to the list.

Olivier: 01:04:49.758 I guess we could have some connections.
[silence]

Xavier: 01:05:04.130 All right. So where do we stand for the structure this week? Because we have lot's of ideas, but I'm not sure we have them in order yet. So we said the-- so the intro, code of conduct, community rules. So here we have I think a good idea of activity with the interactive fiction. Oh, you put it in communication within a project rather than the code of conduct community rules. So what's your thinking? Because all of those different section, like line 60 and 62, it depends how we want to teach it. So what would you put actually in code of conduct community rules if we keep the interactive fiction for communication within the project?

Marc: 01:06:09.279 Basically, I was thinking of community rules as explicit and implicit rules that you want to follow when you interact which is our implicit rules like don't be a jerk, basically, which is usually an implicit rule when you interact with anyone. And also implicit rules that are more specific about means of communications like don't make people waste time-- don't waste people's time. And the code of conduct is an example of an explicit rule which is if you come into this project and interact with other people there might be rules written that you have to follow. And so you can see these rules and it's usually on the form of a code of conduct. But I agree that the organization of this thing is not-- it was just ideas we can completely change it and integrate it into other chapters.
[silence]

Xavier: 01:07:24.293 Yeah, I think, well, maybe the code of conduct community rules might be a subsection of communication within a project, at least the way we approach it here, and.

Marc: 01:07:38.762 We can mix the communication channels with how to behave on that channel before the implicit rules part [inaudible].

Xavier: 01:07:48.099 Sure.

Marc: 01:07:51.367 But we still have to mention that there may be explicit rules to follow our code of conduct that are not specific to a communication channel but usually are applicable to all communication across a project.

Xavier: 01:08:06.846 Okay. All right. So in any case, yeah-- in any case, yeah, there will be structuring those in terms of things to assign. That's what I'm trying to think about here. We'll have those explanations and maybe taking from the section from producing [FLOSS?] about rule to behave, etc. So explanation. And the activity which will be, yeah, maybe two or three short interactive fiction and asking them to do that. So I think that's already two different things. [inaudible] video, yeah, that's good. Diversity of contributors, what is that part? Oh, that's what we mentioned that it might be different type of skills, etc., right?

Olivier: 01:09:15.943 Yeah.

Marc: 01:09:18.037 Or just people who want to ask help about how do I use your project like, "I try to use it and it crashed and what can I do and why? What can I do when I want to do that?" Users are type of potential contributor, but.

Xavier: 01:09:47.641 All right. And if we think about one thing that we consider at the beginning which was to make that the week where we talk a lot about choose your project and etc. So do we end up keeping like the more traditional structure than the other week where we have a few more, not theoretical, but more general sections like communication within a project, retention in project and etc. And then we just do the project tips aspects at the end, maybe making it a little bit bigger that week, or do we try to mix that in? The way it's done now is more like keeping it at the end, which would be fine too. But just to ask that?

Marc: 01:10:31.171 I think it would not be a problem to keep the structure and would provide some stability in the structure of the MOOC.

Kendall: 01:10:40.233 Yeah, I would agree.

Xavier: 01:10:43.474 That fine.

Kendall: 01:10:44.015 And doing that, we could get them to update the journal thing that they've been keeping throughout with their final decision on the project. And if there are multiple communication channels or mailing lists or whatever, we can have them list out the ones that they're connected to, subscribed to, joined, what have you.

Remi: 01:11:04.023 Maybe they can compare and see which ones are the more active.

Marc: 01:11:11.047 Also, we should mention that if they join a mailing list, they don't have to post to it. They can just lurk, and it's what most people do in mailing list. And it's fine to join a mailing list and not send any mail to say, "Hi, I'm new."

Olivier: 01:11:23.022 That's what you should do.

Kendall: 01:11:25.136 Yeah.

Olivier: 01:11:25.842 You should definitely read the archive before posting.

Marc: 01:11:31.605 At least the recent one.

Olivier: 01:11:33.395 About four weeks, maybe.

Xavier: 01:11:35.924 Well, we'll still be encouraging--

Remi: 01:11:38.358 Sorry, Xavier.

Xavier: 01:11:40.452 Just to mention that we'll still be encouraging them to participate because that's going to be the tricky bit. But obviously it's better, just like entering a room, like a party or whatever, if you enter the room and start blurting nonsense to everyone immediately, people are not going to like you. On the other side, if you enter the room and just stick in your corner the whole evening, not much is going to happen either. So I think it will need to be somewhere in the middle.

Kendall: 01:12:09.591 There's a balance to be reached there, yeah.

Olivier: 01:12:14.282 It depends on the music in the background. [laughter] Loud, louder. Did I mention the party of Iceweasel in the late '90s, where they hired a DJ for welcoming the GNOME contributors on a boat on the river river Seine in Paris? And the DJ was committing suicide because no one was dancing because just the music wasn't right. And when he started to play the right music, all the hackers finally left their laptops and started [laughter] jerking, and finally the party was successful. But that's one of the anecdotes of the old times.

Xavier: 01:12:56.061 That is funny. Know your audience, right? All right.

Marc: 01:13:08.358 The part about retention in projects looks like sort of unrelated.

Xavier: 01:13:15.569 I agree. I don't remember exactly the context, I think--

Marc: 01:13:22.102 It was about people coming and going and staying for one commit or staying for years [inaudible].

Remi: 01:13:30.580 [crosstalk].

Olivier: 01:13:31.140 I think it's the first thing to get some not immediate first time contribution to get the bigger picture to say, "Okay. People go and that's not a shame. It's not like the project is not welcoming, but life evolves and so on." So maybe the immediate perspective of the students is, "Okay. The next weeks, how will I do?" But then thinking about years maybe is interesting.

Xavier: 01:13:59.852 Yeah, I think that was the context in which we discussed that is the burnout in the context of projects. And I think it was mentioned in terms of thinking about the fact that the person in front of us, the maintainer and etc., might also be sometimes in that state where they are still there, but they don't really want to be there. So they might be rude because of that. So that can be a factor in the communication. So for them to project themselves in the future and to not let that happen, to basically be able to say, "Okay, I'm done with the project," or, "I don't have time for it anymore. Maybe I'll come back to it later." But also, yeah, considering that depending on the person they might be talking to, they will have a very different experience. And if they are talking to someone who is not being very nice or dismissive or whatever, maybe they want to switch to another person or even to another project. But it's true that I don't know how much we want to talk about it here. I mean, there is a part that is related to communication like I just mentioned. Maybe there is a better week for that. Can you think of another one?

Marc: 01:15:25.839 No. No. It's the only week where we talk about the communities in depth, so.

Xavier: 01:15:34.676 So yeah, that might be like just a side mention.

Marc: 01:15:39.221 We can also mentioning within the visual novels like mentioning that when we introduce participants we can say, "This one has been here for 10 years. This one has been here 2 years ago for one commit and just now is suggesting something new and not planning to stay."

Xavier: 01:16:04.851 Yup. That could be a-- there is even a link around the code of conduct and how to not make other people lose their time is that it's valid on the contributor's side, but the maintainers also might do that sometimes. And like you were saying, Olivier, that the rules tend to be slightly different for the new contributors and the old timers. So the old timers might have more leeway with that. So it's not good to copy that because--

Olivier: 01:16:35.885 I think it would be interesting to present things as a nonbinary and not always precise. I mean, there is also-- it's communities, and communities are not just people that are always motivated by just publishing code or documenting or stuff. So it's also their personal involvement, their hobbies, their family, their friends, and so on. So as always there are periods where people are focused on work and periods where people just want to discuss life and so on, depending on your morale and so on. So you may expect in projects that are not only what we describe by the letter in the course or in the guidelines. So maybe having some opening on, okay, it's a community and people don't always behave in a precise or coherent way.

Kendall: 01:17:33.571 We do kind of go into that a little bit in week one with the section two, starting into FLOSS. We were going to have like the video interviews and make sure to cover a really diverse set of people. People that contribute to open source as part of a university, people that contribute for their job, people that do it on their free time, diversity of language, geographic location, all of those things. So we could point it out more there that the more diverse a open-source community is generally the more successful and resilient it is and bring that up again here, or I don't know, or thoughts.

Xavier: 01:18:22.973 Yeah. That would make sense. Especially because those interviews are things that we talked about potentially sprinkling through the course, so that we can touch base a little bit on it on week one and maybe insist a bit more here. I think there will be a lot of that back and forth between the first weeks and week four because basically the whole first three weeks are a build up to getting to the state we want to be at the end of week four. So yeah, definitely.

Olivier: 01:18:51.455 And all the folklore and trolling so on that is part of the projects when it's done the right way. It's important to give a sense of what it is for people that would have their first contact with online communities this way. I mean, if you never participated in an interest group, online, with people that you don't know and so on, maybe you could be shocked. And so probably we shouldn't do harm to the students, but we should prepare them for something that is more fuzzy, more bizarre, maybe, than what we teach them, or what they can read in books. And then, when they are in the community, when they have selected the project, maybe they can notice messages that seem out of scope or people that don't seem like a maintainer but are still there. I don't know. Like on IRC for instance, for me it's been sometimes like there are all the chans where people work, and sub chans that we create for leisure time. And there are periodically moments when it's not respected, people are just discussing stuff, trolling each other on the main venue.

Marc: 01:20:34.927 Only on Friday.

Olivier: 01:20:34.927 And of course, for the people that don't have the time to read everything it's hard, it's rude. But it's part of the project, so.

Kendall: 01:20:43.096 Yeah. The culture of the project.

Olivier: 01:20:46.045 Yeah, the folklore, the culture, the.

Kendall: 01:20:48.682 Yeah. It's the same thing in OpenStack. We do a lot of trolling each other on IRC. But we all know each other and are friends, so it's okay. [laughter]

Olivier: 01:20:59.705 Yeah, but if you're in the position of a newcomer, it can be intimidating. And the [barrier?] language is quite important in this respect. But also all cultural, all the bias, or the--

Xavier: 01:21:17.249 Yeah, I guess it's true also that it's relevant in the context of talking about code of conducts, and we'll be giving them a lot of rules. The projects have all a lot of rules. But maybe we should also keep that in mind or give that as a counterbalance to not tend to being a stickler with the rules because that's still supposed to be fun, and it's still a community, so yeah. I like that.

Marc: 01:21:40.716 How would you put it in the table?

Xavier: 01:21:45.354 I think it's, again, on that communication within the project that might be-- maybe even just in the section of the code of conduct and rules, we talk about rules, and we can just say, "Okay. Don't go too far with that." It's fine too. One can breathe.

Olivier: 01:22:04.640 Sometimes it's the difference between the project and the product. I mean, if you would troll people in a GitHub issue, it would generally be considered rude. But if you troll people on IRC or Slack or whatever, it's probably fine to a certain extent. So the product could be the issues, and the project could be IRC. Stuff like that, so. And both are, of course, intertwined.

Kendall: 01:22:36.321 Yeah. You should also probably know the person before you start trolling them and stuff, not just like, "Oh, random IRC nickname. You are my victim today." [laughter] No.

Olivier: 01:22:48.510 And when you do-- when you say you should know people, actually, sometimes you know only a nick. Until the next conference, where you finally have beers together, hopefully.

Xavier: 01:23:05.771 But that can be a form of knowing, though. But yeah, I agree. I think it ties, again, into that aspect of making sure that we understand people's perception of our actions. It's really key when you want to join a community to not just be doing whatever, but really trying to perceive how will be perceived. Being comfortable to troll people and that they are comfortable trolling us back might be a nice goal, but obviously not something that we'll do as a first action as a newcomer, yeah.

Olivier: 01:23:44.309 How to troll the right way. [laughter]

Xavier: 01:23:49.739 All right. Because we are at the end, maybe we want to finish that and the assignation also.

Marc: 01:23:58.430 We want [change?] in formats, and in person in charge.

Xavier: 01:24:02.236 Yes. Da, da, da, da. So I think the mentorship might also be part of the checklist. That checklist actually is part of-- it's the project tips activity, I think. [inaudible] for that.

Marc: 01:24:31.883 So we don't have any activities until the game?

Xavier: 01:24:39.502 Well the game could be something we start with, actually. We could have one interaction, let them choose, let it go pretty badly. And then that give them the motivation to learn more about that, or something. And I have more of those at the end, especially because we want to have them contributing some. So we could always be presenting something, having an interactive fiction that present that point. Then another one. Then this way we get the two or three interactive fiction sprinkled through the communication within the project. And at the end of that, having them maybe contributing one of their own if they want to. Would that make sense?

Marc: 01:25:32.391 So you want to spread the game across all the items basically, or?

Xavier: 01:25:38.942 Yeah, as an illustration of each of the points. And I think it was mainly, to answer your first point, that we waited a long time before getting the first activity. It could also be a good way to start, basically, by putting them in a situation. And if you think about it, having a week or module that start with something like a interactive fiction, as a student, I would probably like that, so.

Remi: 01:26:04.621 Okay.

Marc: 01:26:05.142 Okay.

Olivier: 01:26:06.658 So I understood that Remi is going to develop the interactive fiction? That's it?

Xavier: 01:26:10.639 Yeah, thank you, Remi.

Marc: 01:26:11.254 He already did.

Remi: 01:26:12.832 I'm going to--

Marc: 01:26:13.391 He put the GitHub link, and.

Remi: 01:26:15.592 I'm going to start--

Xavier: 01:26:16.770 You said you had a way to do it very easily. So it's good for you, right?

Remi: 01:26:21.342 I will install the server, and you will use the server to edit the story yourselves.

Xavier: 01:26:30.793 I mean, I can contribute one of the stories. That seems fun. If we could have several people, maybe like two or three, one per story, that could be nice as well.

Olivier: 01:26:38.401 Are we allowed to kill the characters in the course of the--?

Xavier: 01:26:41.708 Sure. Go ahead.

Remi: 01:26:45.659 I have a friend that made a PhD in our school about algorithm that generates these kind of stories, like story telling, automatic generation, fiction generation. And he's an expert on this kind of scenario. I can ask him [crosstalk]--

Olivier: 01:27:06.421 And now he's working for Emmanuel Macron at the presidency.

Remi: 01:27:11.017 No, but for big company in France that is close to him.

Olivier: 01:27:19.361 It's recorded. Beware.

Remi: 01:27:22.449 Okay.

Xavier: 01:27:24.390 So just to finish that because we are at the end. So here we have everyone for person in charge of the interactive fiction. That's generally the best recipe so that nobody does it. So it could be maybe better if we had some explicitly interested people. So Remi, you mentioned at least installing it. Would you want to do one of the interactive?

Remi: 01:27:47.282 Yes. Yes.

Xavier: 01:27:48.500 Okay, so we'll [inaudible]--

Remi: 01:27:49.994 I can try to have a proof of concept, let's say.

Xavier: 01:27:55.193 Cool. Anyone else? If we could have a third one, that would be great. We would have three stories. [crosstalk]--

Marc: 01:28:02.180 I would work with Remi. When he installs it, I will help him.

Xavier: 01:28:07.169 Marc, also. Kendall, Olivier?

Olivier: 01:28:10.705 No, configuring the NGINX is no help, Marc.

Xavier: 01:28:16.920 Okay. So I'll put also Marc and Kendall. And that can be a start, but obviously, anyone else can add themself there. On my side for the stuff to take, I can take the coordination of this week if that's good. I think, Marc, you were also interested, if that's good with you.

Marc: 01:28:42.375 Yes, that's good with me. You can leave it. Did you want also to do the project tips since you did those before, or you want to not take this one because they are bigger, or?

Xavier: 01:28:55.169 No. That's good especially because all the previous one are going to tie into this one, so that's good actually that I take them.

Olivier: 01:29:05.845 I'm sorry, I cannot really volunteer for serious work unless-- no, well.

Xavier: 01:29:12.762 Only trolling.

Marc: 01:29:16.033 Did you see the item you were on?

Olivier: 01:29:19.009 Yeah, more or less.
[silence]

Marc: 01:29:32.638 And were you interested in that, or? There were two. One about trolls in week three and one about history in week one.

Olivier: 01:29:53.252 In principle, I could try to do something like writing slides, and. But I don't know exactly when, how.

Marc: 01:30:10.031 There is no rush. So if it's of interest to you, you can tell that if interests you. And if it doesn't interest, you just tell no.

Olivier: 01:30:21.601 It would be good that I do something, but at the moment it's hard.

Marc: 01:30:27.935 It doesn't have to be in the next month.

Olivier: 01:30:29.934 At the moment, and for the next years.

Marc: 01:30:37.956 Okay, so.

Olivier: 01:30:38.414 But again, I subscribe to GitLab hoping that GitLab would prompt me sometimes by email that I have things to do, or that I shouldn't.

Marc: 01:30:45.614 Okay, put in a [crosstalk]--

Olivier: 01:30:46.632 And then it's not in GitHub anymore, how could I do?

Marc: 01:30:50.909 I will leave you subscribed, and then we will ping you in GitLab.

Xavier: 01:30:54.617 Yeah. Be careful what you wish for because I definitely feel that you'll get some things.

Marc: 01:30:58.875 Anyway, we should have more people in charge for the third items, so I can probably take the intro. And we have to talk about the code of conduct.

Remi: 01:31:18.392 I put myself in the project, Get In Touch. Maybe I was thinking about a peer grading activity where they have to prove they tried to get in touch.

Xavier: 01:31:32.368 So which part is--? Oh, project Get In Touch here. Yeah, and that equals what you take care of in the first week also. So that's a good idea.

Marc: 01:31:50.822 Okay. Kendall, is there anything on this week that motivates you, or?

Kendall: 01:32:04.499 I'll look through the thing again. Aside from writing the interactive story, I could probably help with just the general communication channels content. I don't know exactly what we would do for video, but I don't know.

Marc: 01:32:27.819 It can be just text. It doesn't have to be [crosstalk].

Kendall: 01:32:30.269 Yeah, here, I'll put my name down for these parts.

Marc: 01:32:41.813 Okay, I will probably put myself on the two points. That's [inaudible]. Diversity of contributors it's-- I would put myself at-- no I can't put myself, but if someone wants-- no, I will put myself, and I will probably enroll someone else from Inkscape project. Retention in project, I don't know yet. I don't know if--

Olivier: 01:33:17.403 I tried to find back the kind of visualization that we're thinking about, about retention and the generations of developers and stuff like that because I'm puzzled. Google is my friend, but I couldn't find it.

Marc: 01:33:36.186 Okay, I put your name so that you will get [inaudible].

Olivier: 01:33:39.572 Yeah, put my name. That's not an essential part of the week, so.

Remi: 01:33:44.956 Is it linked to Gource?

Olivier: 01:33:49.582 Gource was something like that but it's more microscopic. You see it in the frame of one week like who's doing what visually. Here was more thinking about like for the 10 years you can see there was first one generation then another, and a few people that are connecting them. So that's a different-- it's not an animation. It's just a picture.

Marc: 01:34:20.514 Okay. And do we end with a quiz, like a general quiz? Quiz time.

Olivier: 01:34:33.473 Did you marry someone in the project? Yes, no?

Marc: 01:34:42.463 If there has been four weeks, probably not.
[silence]

Olivier: 01:35:01.731 Question. Is there anywhere in the previous weeks where we speak about physical-- sorry, physical--

Marc: 01:35:12.096 Meetups.

Olivier: 01:35:12.896 --meetups, conferences, beers, pizzas, and stuff?

Marc: 01:35:18.320 No.

Xavier: 01:35:19.436 I don't think so.

Marc: 01:35:20.744 This week would be the place to talk about it.

Olivier: 01:35:23.895 Because well, in times of pandemics of course everything has gone quite virtual or online I would say. But in the life of most open-source projects, in the end pretty much important to have physical gatherings and stuff like that.

Remi: 01:35:44.319 That's very interesting.

Olivier: 01:35:45.483 And organizing them is a very valuable contribution also. So maybe that's something we could--

Xavier: 01:35:54.873 Maybe it could go in communication channels. Because even if it's not a continuous one, it's a way to get in touch with the community, so.

Olivier: 01:36:04.463 And of course, if you don't participate then you don't understand all the trolls and so on. Or if you can't participate. If you don't have the funding then there is a bias and so on and so forth.

Remi: 01:36:14.194 That's right. Many of the very big projects, they have an attending physical conference. Like the Open edX conference, Xavier.

Xavier: 01:36:26.375 Oh yeah. Definitely, if you don't go to the conference every year or at least from time to time, it's difficult to understand what's going on.

Olivier: 01:36:36.712 I remember speaking with the team, the technical team at FUN, France Universite Numerique, before they went to the conference in, [inaudible] I guess. And after. And apparently, they were just outsiders. And after they have managed to get to know each other physically then-- that's the perception I have, or the recollection I have. And suddenly they counted each other and they were more numerous than the American folks or another sub-community. And that was a reckoning that couldn't have happened if they didn't participate to the conference. Stuff like that. Maybe that shouldn't be said on a recorded call, but.

Marc: 01:37:28.856 If you've finished then I can stop the recording and we can continue discussing.

Remi: 01:37:32.832 [crosstalk].

Olivier: 01:37:33.608 And I can go and have lunch or dinner.

Xavier: 01:37:36.388 Yeah. I need to go also actually.

Remi: 01:37:40.445 Have a breakfast, yes, Xavier?

Xavier: 01:37:42.775 Yes. Exactly. Time for breakfast. Well, that was good. I think we've got a good plan now for that, so.

Marc: 01:37:51.113 Yeah. We started without any plan for this week, and I think we ended up-- the idea of visual novels is I think a great one. That was not anticipated.

Olivier: 01:38:02.828 Is it that one or something like a Among Us which wouldn't be as interactive? I fear the complexity of writing a scenario, writing the [inaudible].

Xavier: 01:38:20.077 Yeah. That's why it needs to remain small otherwise that gets out of control quickly.

Remi: 01:38:25.116 I can ask some advice from my friend and he will have some ideas.

Xavier: 01:38:32.316 Would be great.

Remi: 01:38:33.395 You're going to have branches but that they merge again some [crosstalk].

Olivier: 01:38:35.953 Yeah. That's the trick.

Xavier: 01:38:39.152 It's like choose your own adventure. I mean we have all dreamed of writing one of those being a kid, so now is our chance.

Olivier: 01:38:47.853 I wanted to do such a project for learning web development with students of mine a few years ago.

Remi: 01:38:56.194 With which tool?

Olivier: 01:38:57.882 Not the one that you pointed to. I can't remember. But that was a weird way to do because basically you would interact with an API, and you would have to pass the headers to understand the plot or to discover secrets.

Marc: 01:39:21.378 Well, a choose your own adventure is a perfect way-- coding that is a perfect way to learn about cookies and stuff like that.

Olivier: 01:39:27.992 Yes. That was trick. Yeah. If you looked at the [foreign] maybe.

Marc: 01:39:35.678 I will stop the recording then.

Olivier: 01:39:37.438 Yes. That's a bit out of topic.

Marc: 01:39:41.437 So thank you, everyone. And I will cut the recording now.

Olivier: 01:39:44.932 And it's 99. So you cut at 99.99?

Marc: 01:39:49.631 I cannot because there are not 99 seconds. But 99.59 if you want.

Olivier: 01:39:56.377 Cut.

Remi: 01:39:58.396 59.
