# Module 4 - Brainstorm, Sorted by Section

* Module coordinator: Xavier
* Length: TBD

## Intro: the social side of FLOSS

Assignee: Marc
Type: video

* Module dedicated to the human interactions within a project, how people interact with each other, the relationship within a project, code of conduct, diversity issues, as well as , where they interact (IRC/chat, mailing lists, etc.).
* The majority of the effort to contribute successfully involves social interactions, rather than technical matters. It is normal to spend a lot of time dealing with human interactions, rather than solving hard technical problems.
* In the previous modules we got in touch with the project, which is a light interaction -- to be able to contribute successfully, we'll need to get involved more deeply with the community.
* A frequent mistake by new contributors is to think that just opening a pull request is enough to contribute. That misses all the work that comes before and after that, and which is highly social in nature:
  * Before, we need to figure out how the project works, what people's motivations are, what type of work they will accept in the project, etc.
  * After, being reactive to address comments from the reviewers and respond, understand what the reviewers really care about to be able to agree on a solution with them when they raise issues, 
* It's even more important when we plan to become a long-term contributor. But even for a single contribution, that social aspect is pretty central.

## Communication within a project

### Code of conducts

Assignee: Marc

* Nowadays most free software projects and communities have them, except some small self-mediated communities that haven't had the need to formalize these rules yet. It has become one of the funding steps of a community, like the choice of a license.
* Also includes other documents describing the rules for interacting within the project, like a `CONTRIBUTORS.md` document
* The social rules to interact, they set expectations. We should not overlook them when joining a project. Project members wrote them for a reason and as a new member, it's important to understand and follow them.
* Part of those reasons is the need to correct for a lack of diversity, which generated bias and discriminatory attitudes towards those not part of the majority. Which is usually white males in their 30s, from Europe and North America, focused mainly on tech and code. If you're not from that group, or not contributing code, it was often considered that the person was worth less, and could be treated accordingly. Just like the origin of a contributor should not be a factor of discrimination, the type of contribution should not either: code, documentation or [design contributions](https://opensourcedesign.net/) are equally important.
* They are the "explicit" rules though, they don't include *all* the rules. They usually describe more of an ideal world and rarely include practical advice, and some remain implicit, eg. around trolling, community interactions, etc
* The code of conduct is the last resort when everything else didn't work. Referring to a code of conduct and respecting it is a useful step, but not sufficient by itself. You know you're doing it right when nobody needs to refer to it, i.e. when the mediation ahead of the rules enforcement is sufficient. Cf example of the LibreOffice community: they have a code of conduct, but they almost never use it.
* Newcomers to a project can be easy targets for abusive behaviors, so it's important to recognize those situations. Being new to a community, there is a strong incentive to try fit in, and it's harder to know what's exactly accepted behavior or not. As newcomers, we generally want to avoid looking confrontational or problematic, so questioning the behavior of established community members can be difficult -- for fear of getting our questions left unanswered, or our contributions unmerged. When we are the recipient of abuse, there can be a lot of pressure to just comply and go with it.
  * It's thus important to identify whether there is someone within the community who is going to make sure that people who are a bit hostile or aggressive do not bother newcomers. Having someone handling the type of mediation mentioned above can make a huge difference, and ensures that contributing doesn't become an unpleasant experience.
  * It's also important to keep in mind that feedback about the atmosphere of a community is a contribution - one of the most difficult ones, actually. It's harder for established members to remember how it feels like to be a newcomer in their community.
  * And remember that if a community is problematic, walking away is always an option, and a form of feedback by itself.
* Provide examples: avoid de-anonymizable examples, but make sure to provide a diversity of honest examples, so that people can know what to expect and be prepared for it, and don't discover it later on by themselves, once they are involved more personally
  * Linus case where he admitted that social issues *are* an issue, and he changed his behavior
  * Loic's example: A mentor in a Google Summer of Code project in a free-software project, tasked to select proposals from candidates. The mentor adopted a different attitude than with the rest of the project members, and was talking to them briskly. Not insulting them, but presenting a very different face than the nice, polite, attentive attitude he had with others. But nobody made any remark about it: it was not completely out of line, and yet despite having a strict code of conduct in the project, and project members with a strong will to enforce it, it was making some people uncomfortable. It was missing someone responsible for diffusing tensions, because that was not a case where the code of conduct was applied.
  * Also ask the participants for examples? Some of them could then be incorporated in the course.

* TODO: Establish a code of conduct for the course
* TODO: Explicitly call for contributors and learners that will improve the project's diversity to join it and participate (and setup an internship with Outreachy?)
* TODO: Get feedback from participants about the inclusivity and diversity of the course -- especially the first impressions

### "How to ask questions the smart way"

Assignee: Marc
Type: mostly text

References:
* http://www.catb.org/~esr/faqs/smart-questions.html
* https://producingoss.com/en/setting-tone.html
* https://producingoss.com/en/communications.html

### How to find the official communication channels & processes of the project

Assignee: Kendall 	
Type: text, video?

TODO: This section would fit more the "Where?" module (module 2) -- to move there?

* IRC/ML/etc
* IRL confs/meetups

* Maybe instead detail here how different methods of communication work
  * Activity/visual novel example: Don't open an IRC channel, ask is anyone around, and leave after one minute

### Examples of interactions

Assignees: remi, xavier, marc, kendall
Type: "game" / activity

* Example of possible chats, guess what are the intentions of participants			
* Visual novel / interactive fiction ?
  * https://github.com/inkle/ink | https://www.inklestudios.com/ink/web-tutorial/

### Diversity of contributors

Assignee: Marc	
Type: text, interviews

## Retention in projects

Assignee: Obergix	
Type: text/video/interviews

References:
* [Why Do Episodic Volunteers Stay in FLOSS Communities?](https://dl.acm.org/doi/10.1109/ICSE.2019.00100)
  * _"Some factors which affect retention cannot be controlled: the popularity of the project, and how early in the life-cycle a developer joins [8], [9]. However, there are also measures that communities can take to encourage retention. For example, modular code and early social interactions with peers are both associated with retention [8], [10]"_
* [An empirical verification of a-priori learning models on mailing archives in the context of online learning activities of participants in free\libre open source software (FLOSS) communities]()
  * [Burn outs, role transition in floss communities](https://www.researchgate.net/profile/Patrick-Mukala/publication/313254065/figure/fig1/AS:961423358304278@1606232599177/Role-transition-in-FLOSS-communities-From-Glott-et-al-2011_W640.jpg)
  * [Workflow net for novice during initiation phase](https://www.researchgate.net/profile/Patrick-Mukala/publication/313254065/figure/fig3/AS:961423358304279@1606232599420/Workflow-net-for-novice-during-the-initiation-phase_W640.jpg)

## Project get-in-touch

Assignee: remi	
Type: peer-grading activity

* TODO: Reconcile with the section in module 1? Move this there, and link back to it when people have changed project, so they make sure to do this step?

* Go to find the IRC channel (or Slack/Mattermost/Rocket.Chat/Matrix/Zulip/mailing-list/etc.), and either:
  * Ask a question
  * Say "thanks!"
  * Answer someone's question
* Make sure it's an interaction - ie that someone else responds, and that there is a little bit of back and forth. The idea is to break the ice, and keep it going.
* It's a difficult and awkward step. Often, we don't know what to ask, and it feels artificial. But it's useful to figure out how to mingle, and to mingle as early as possible:
  * It's like being in a party for the first time with lots of people we don't know. We go to someone, a glass in our hand, and we engage the conversation. The topic doesn't need to be brilliant, but it's important for the interaction to be appealing to the person we talk to.
  * The more we wait in our corner without talking to anyone, the more difficult it looks to do that. When we start talking to someone immediately, it might at first be a very superficial conversation, but the earlier we can get to more interesting topics - simply become we get to know each other earlier.
  * It's also similar to the role of networking when looking for a job. To get recommended for jobs (or aware of good opportunities), we need people to know us, to trust us -- this is also true in the context of an open source project, to be known and trusted enough to warrant the time and attention of maintainers, as well as increase their goodwill to accept our work.

* Someone needs accept our first contribution, which means that someone will need to be paying attention to what we submit when we submit it. And it's more likely to happen with people we know for a longer time: someone we've known even slightly for four weeks is more likely to accept our contribution  than someone we've met yesterday. When the time comes to accept our contribution, we need to be already part of the team somehow, even just a little bit. Being around for some time, interacting with other community members, is the simplest way to achieve that.
* Interacting with the project's community directly also develops our own understanding of the project. We are social animals, so social interaction is a huge part of knowledge acquisition within a group. By having put ourselves out there, by having interacted, we'll also adapt how we contribute, and what we contribute -- making it also likelier to be accepted than if we had waited.
  * It also develops our empathy and understanding of the individuals who manage a project. For example, an domain expert who comes to contribute to a free software project, they might assume that some the project contributors are hobbyists, and might not take them very seriously. But by interacting directly, rather than just looking at the project from a distance, there will be answers and pushback. There will be times where the expert make mistakes too -- so it helps the experts to have a more friendly attitude toward people who are less skilled, the interaction brings everyone closer.
  * There is also the opposite case, where a new contributor will assume that everyone in the project is an expert, and end up producing code that is too complicated for the project to be able to maintain. Interacting with the project members early and frequently helps to make sure that the other people from the project will be able to understand and maintain the code being produced.

* Ask the students to check if they indeed tried to get in touch ?
* The proof : screenshot ? and update your gitlab project

## Project tips

Assignee: Xavier

### Finalizing the project selection

* Last chance -- ideally participants will have already engaged with a project for several weeks at this point. They can still switch now, but it's their last chance to do so
* GO OR NO GO decision: If the project isn't a good candidate for contributions though, we want to make sure the learners can detect it, and can use this last opportunity to find a better one
* TODO: Provide checklist to grade the selected project (go through the whole course content once finished to compile it)
  * Use methodologies to evaluate the strength and weaknesses of FLOSS communities:
    * [CHAOS](https://chaoss.community/metrics/)
    * https://carlschwan.eu/2021/04/29/health-of-the-kde-community/
    * https://opensource.com/business/15/12/top-5-open-source-community-metrics-track
  * Pace of responses/interactions in the community and interaction times/timezones -- has to match the learner's
  * Size of the community -- big ones are *usually* easier to onboard, as they are more used to onboarding)

### Contribution communication levels

Not all contribution types require the same level of communication complexity. As mentioned in module 1, we want to start from the smallest size, and slowly work our way towards the larger, more complex levels, especially with social aspects:

1. Answering a question on the mailing list or IRC from someone who asks it
1. Fixing a typo in the documentation
1. Fixing a easy simple bug in trivial way
1. Fixing a more complex bug
1. Developing a feature
1. Reorganizing the governance of the project

TODO:
* Reconcile with the previous modules/sections -- implement the first 2-3 steps as activities at the end of each module, to progressively ramp up the contributions?
* Make sure learners have completed those preliminary contribution steps

### How to respond to reviews

* A simple rule applies: obeying the reviews that we get, even when we don't necessarily agree with them. As newcomers, we submit ourselves to the reviewer to the extent that our work is not degraded beyond recognition.
* Maybe we would have done things differently, but as long as the review does not go against our contribution, having to adapt to the requests of the reviewers will still bring us closer to our goal. It's better to have our a modified version of our contribution merged, rather than disagreeing and ending up with an unmerged contribution, which we'll have to maintain separately forever.
* This is a social interaction many of us struggle with at first -- it can be difficult to do something without being convinced that it is the right approach, or for matters of taste, like the style of a function. But being newcomers, we don't yet have much credit in the community or the reviewers, so accepting and applying all review comments will help to score precious points in convincing reviewers to accept the contribution.
* Once we know the project members better, and we have more credit as a recurring contributor, we will have more latitude to address controversial topics -- but we first need to prove that we can work with the existing community of contributors. This is similar to the contributions levels described in the previous section.
* This is similar to joining a new job, or a party from people we don't know: if the host asks you to help set the table, and wants it done in a specific way, it would be rude to refuse or criticize the approach. Being at someone's place and not knowing anyone, we are expected to conform to their way of doing things -- and if we don't like it, we can always go home early. After a while, once we have a good relationship with the host, maybe then we can start to say, "Hey, I'm not sure about the way set the table, maybe there is a better way?"

### Mentorship

* Last chance to find a mentor or pick a group for peer-working
* TINDER-like matching?

### Contribution journal update

* Update journal with (include links to relevant sections/modules):
  * Project (what?)
  * Locations (where?) and especially communication channels being monitored (MLs, IRC channels, etc)
  * Actors of the projects (who?)
  * How:
    * How the project works (points from the current document: code of conduct
  * Potential contributions details/ideas (type of contribution, area/repos identified, core/plugin, etc.)
  * Mentor or mentoring resources if you have one already
  * Mentor/peers

## Quizz time

Assignee: Marc (probably)	
Type: graded activity (MCQ?)

## References & papers

TODO:
* Section to sort
* Research additional papers about free software communities and contributions on Google Scholar
* Contact some of the authors we find in those papers just to let them know we are working on that

References:
* [Towards_a_theory_on_the_sustainability_and_performance_of_FLOSS_communities](https://www.researchgate.net/publication/4161854)
* [Community Data Science Collective](https://wiki.communitydata.science/Main_Page): social scientist groups studying online communities and especially open source communities like Wikipedia, Linux, etc., from North American universities.

